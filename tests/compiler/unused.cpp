// Copyright © 2015 Lénaïc Bagnères, hnc@singularity.fr

// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.


#include <iostream>

#include <hopp/test.hpp>
#include <hopp/conversion/to_string.hpp>
#include <hopp/compiler/unused.hpp>


void f(int const i)
{
	hopp_unused(i);
}


int main()
{
	std::cout << "Test #include <hopp/algo/compare_range.hpp>" << std::endl;
	std::cout << std::endl;
	
	int nb_test = 0;
	
	f(42);
	
	hopp::test(nb_test == 0, "hopp_unused: " + hopp::to_string(nb_test) + " test(s) fail(s)!\n");
	
	return nb_test;
}
