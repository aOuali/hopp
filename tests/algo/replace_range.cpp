// Copyright © 2013, 2015 Lénaïc Bagnères, hnc@singularity.fr

// This file is part of hopp.

// hopp is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// hopp is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU Affero General Public License for more details.

// You should have received a copy of the GNU Affero General Public License
// along with hopp. If not, see <http://www.gnu.org/licenses/>


#include <iostream>
#include <vector>
#include <list>
#include <string>

#include <hopp/test.hpp>
#include <hopp/conversion/to_string.hpp>
#include <hopp/algo/replace_range.hpp>
#include <hopp/print/std.hpp>


int main()
{
	std::cout << "Test #include <hopp/algo/replace_range.hpp>" << std::endl;
	std::cout << std::endl;
	
	int nb_test = 0;
	
	// Empty
	
	++nb_test;
	{
		std::vector<int> c(0);
		hopp::replace_range(c, c.begin(), c.end(), c.begin(), c.end());
		nb_test -= hopp::test
		(
			c == std::vector<int>(0),
			"hopp::replace in an empty vector fails\n"
		);
	}
	
	++nb_test;
	{
		std::list<int> c(0);
		hopp::replace_range(c, c.begin(), c.end(), c.begin(), c.end());
		nb_test -= hopp::test
		(
			c == std::list<int>(0),
			"hopp::replace in an empty list fails\n"
		);
	}
	
	// string
	
	++nb_test;
	{
		std::string src  = "original std::string!";
		std::string dest = "same size std::string";
		std::string c = src;
		hopp::replace_range(c, c.begin(), c.end(), dest.begin(), dest.end());
		std::cout << src << "\n" << c << std::endl;
		nb_test -= hopp::test
		(
			c == "same size std::string",
			"hopp::replace fails\n"
		);
	}
	std::cout << std::endl;
	
	++nb_test;
	{
		std::string src  = "a std::string";
		std::string dest = "a std::string replaced";
		std::string c = src;
		hopp::replace_range(c, c.begin(), c.end(), dest);
		std::cout << src << "\n" << c << std::endl;
		nb_test -= hopp::test
		(
			c == "a std::string replaced",
			"hopp::replace fails\n"
		);
	}
	std::cout << std::endl;
	
	++nb_test;
	{
		std::string src  = "a std::string";
		std::string dest = "replaced!";
		std::string c = src;
		hopp::replace_range(c, c.begin(), c.end(), dest.begin(), dest.end());
		std::cout << src << "\n" << c << std::endl;
		nb_test -= hopp::test
		(
			c == "replaced!",
			"hopp::replace fails\n"
		);
	}
	std::cout << std::endl;
	
	++nb_test;
	{
		std::string src  = "a std::string wtih a typo";
		std::string dest = "with";
		std::string c = src;
		hopp::replace_range(c, c.begin() + 14, c.begin() + 14 + 4, dest);
		std::cout << src << "\n" << c << std::endl;
		nb_test -= hopp::test
		(
			c == "a std::string with a typo",
			"hopp::replace fails\n"
		);
	}
	std::cout << std::endl;
	
	++nb_test;
	{
		std::string src  = "a std::string";
		std::string dest = "";
		std::string c = src;
		hopp::replace_range(c, c.begin() + 2, c.begin() + 2 + 5, dest.begin(), dest.end());
		std::cout << src << "\n" << c << std::endl;
		nb_test -= hopp::test
		(
			c == "a string",
			"hopp::replace fails\n"
		);
	}
	std::cout << std::endl;
	
	++nb_test;
	{
		std::string src  = "a string";
		std::string dest = " std::";
		std::string c = src;
		hopp::replace_range(c, c.begin() + 1, c.begin() + 1 + 1, dest);
		std::cout << src << "\n" << c << std::endl;
		nb_test -= hopp::test
		(
			c == "a std::string",
			"hopp::replace fails\n"
		);
	}
	std::cout << std::endl;
	
	hopp::test(nb_test == 0, "hopp::replace: " + hopp::to_string(nb_test) + " test(s) fail(s)!\n");
	
	return nb_test;
}
