// Copyright © 2016 Lénaïc Bagnères, hnc@singularity.fr

// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.


#include <iostream>

#include <hopp/test.hpp>
#include <hopp/conversion/to_string.hpp>
#include <hopp/container/tree.hpp>


int main()
{
	std::cout << "Test #include <hopp/container/tree.hpp>" << std::endl;
	std::cout << std::endl;
	
	int nb_test = 0;
	
	{
		hopp::tree<int> t;
		std::cout << t << std::endl;
	}
	
	{
		hopp::tree<float> t(5, 42.f);
		std::cout << t << std::endl;
	}
	
	// TODO
	
	hopp::test(nb_test == 0, "hopp::tree: " + hopp::to_string(nb_test) + " test(s) fail(s)!\n");
	
	return nb_test;
}
