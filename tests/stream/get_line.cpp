// Copyright © 2014, 2015 Lénaïc Bagnères, hnc@singularity.fr

// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.


#include <iostream>
#include <sstream>
#include <string>

#include <hopp/test.hpp>
#include <hopp/conversion/to_string.hpp>
#include <hopp/stream/get_line.hpp>


int main()
{
	std::cout << "Test #include <hopp/stream/get_line.hpp>" << std::endl;
	std::cout << std::endl;
	
	int nb_test = 0;
	
	++nb_test;
	{
		std::stringstream ss("Lines separated by | or ; delimiters \n (end)");
		
		std::string line_0; hopp::get_line(ss, line_0, { '|', ';' });
		std::string line_1; hopp::get_line(ss, line_1, { '|', ';' });
		std::string line_2; hopp::get_line(ss, line_2,  { '|', ';', '\n' });
		
		std::cout << "Line 0 = " << line_0 << std::endl;
		std::cout << "Line 1 = " << line_1 << std::endl;
		std::cout << "Line 2 = " << line_2 << std::endl;
		
		nb_test -= hopp::test
		(
			line_0 == "Lines separated by " &&
			line_1 == " or " &&
			line_2 == " delimiters ",
			"hopp::get_line fails\n"
		);
	}
	std::cout << std::endl;
	
	hopp::test(nb_test == 0, "hopp::get_line: " + hopp::to_string(nb_test) + " test fail!\n");
	
	return 0;
}
