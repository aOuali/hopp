// Copyright © 2014, 2015 Lénaïc Bagnères, hnc@singularity.fr

// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.


#include <iostream>
#include <sstream>
#include <string>

#include <hopp/test.hpp>
#include <hopp/conversion/to_string.hpp>
#include <hopp/stream/ignore_whitespaces.hpp>


int main()
{
	std::cout << "Test #include <hopp/stream/ignore_whitespaces.hpp>" << std::endl;
	std::cout << std::endl;
	
	int nb_test = 0;
	
	++nb_test;
	{
		std::stringstream ss("  \t\t \n  \n\t  Some white-spaces");
		hopp::ignore_whitespaces(ss);
		nb_test -= hopp::test(char(ss.peek()) == 'S', "hopp::ignore_whitespaces(\"...Some white-spaces\") fails\n");
		std::string line; std::getline(ss, line);
		std::cout << "ss = \"" << line << "\"" << std::endl;
	}
	std::cout << std::endl;
	
	++nb_test;
	{
		std::stringstream ss("  \t\t \n  \n\t  ");
		hopp::ignore_whitespaces(ss);
		nb_test -= hopp::test(ss.good() == false, "hopp::ignore_whitespaces(\"...\") fails\n");
		std::string line; std::getline(ss, line);
		std::cout << "ss = \"" << line << "\"" << std::endl;
	}
	std::cout << std::endl;
	
	hopp::test(nb_test == 0, "hopp::ignore_whitespaces: " + hopp::to_string(nb_test) + " test fail!\n");
	
	return 0;
}
