# Copyright © 2015 Lénaïc Bagnères, hnc@singularity.fr

# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.


# Try to find the hopp library

# HOPP_FOUND       - System has hopp library
# HOPP_INCLUDE_DIR - The hopp include directory

# See other lib/hopp_*/*.cmake files for other variables


find_path(HOPP_INCLUDE_DIR "hopp/hopp.hpp")

if (HOPP_INCLUDE_DIR)
	
	set(HOPP_FOUND "TRUE")
	
	include_directories(${HOPP_INCLUDE_DIR})
	
	message(STATUS "Library hopp found =) ${HOPP_INCLUDE_DIR}")
	
else()
	
	set(HOPP_FOUND "FALSE")
	
	message(STATUS "Library hopp not found :(")
	
endif()


find_package(hopp_linux)

find_package(hopp_openmp)

find_package(hopp_openssl)

find_package(hopp_os_x)

find_package(hopp_unix)

find_package(hopp_windows)
