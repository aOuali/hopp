# Copyright © 2015 Lénaïc Bagnères, hnc@singularity.fr

# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.


# Try to add custom target to generate API documentation with Doxygen

# HOPP_DOXYGEN_FOUND        - System has Doxygen executable
# HOPP_DOXYGEN_EXECUTABLE   - Doxygen executable

# DOXYGEN_WORKING_DIRECTORY - Doxygen working directory is set by the user

# HOPP_DOXYFILE             - Doxyfile file


find_package(Doxygen)

if (DOXYGEN_FOUND)
	
	set(HOPP_DOXYGEN_FOUND "TRUE")
	set(HOPP_DOXYGEN_EXECUTABLE "${DOXYGEN_EXECUTABLE}")
	
	message(STATUS "Doxygen found =) ${HOPP_DOXYGEN_EXECUTABLE}")
	
else()
	
	set(HOPP_DOXYGEN_FOUND "FALSE")
	
	message(STATUS "Doxygen not found :(")
	
endif()



if (DOXYGEN_WORKING_DIRECTORY)
	message(STATUS "DOXYGEN_WORKING_DIRECTORY is set =) ${DOXYGEN_WORKING_DIRECTORY}")
else()
	message(STATUS "DOXYGEN_WORKING_DIRECTORY is unset :(")
endif()


file(GLOB_RECURSE HOPP_DOXYFILE Doxyfile)

if (HOPP_DOXYFILE)
	message(STATUS "Doxyfile found =) ${HOPP_DOXYFILE}")
else()
	message(STATUS "Doxyfile not found :(")
endif()


if (DOXYGEN_FOUND AND DOXYGEN_WORKING_DIRECTORY AND HOPP_DOXYFILE)
	
	add_custom_target(
		doxygen
		${DOXYGEN_EXECUTABLE}
		${HOPP_DOXYFILE}
		WORKING_DIRECTORY "${DOXYGEN_WORKING_DIRECTORY}"
		COMMENT "Generating API documentation with Doxygen" VERBATIM
	)
	
endif()
