# Copyright © 2015 Lénaïc Bagnères, hnc@singularity.fr

# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.


# Try to find the OpenSSL library

# HOPP_OPENSSL_FOUND       - System has OpenSSL library
# HOPP_OPENSSL_INCLUDE_DIR - The OpenSSL include directory
# HOPP_OPENSSL_LIBRARY     - Library needed to use OpenSSL

# HOPP_WITH_OPENSSL_MACRO  - System has OpenSSL library (macro)


find_package(OpenSSL)

set(HOPP_WITH_OPENSSL_MACRO "hopp_with_openssl")

if (OPENSSL_FOUND)
	
	set(HOPP_OPENSSL_FOUND "TRUE")
	set(HOPP_OPENSSL_INCLUDE_DIR "${OPENSSL_INCLUDE_DIR}")
	set(HOPP_OPENSSL_LIBRARY "${OPENSSL_LIBRARIES}")
	
	include_directories(${HOPP_OPENSSL_INCLUDE_DIR})
	
	add_definitions("-D${HOPP_WITH_OPENSSL_MACRO}")
	
	message(STATUS "Library OpenSSL found =) ${HOPP_OPENSSL_INCLUDE_DIR} | ${HOPP_OPENSSL_LIBRARY}")
	
else()
	
	set(HOPP_OPENSSL_FOUND "FALSE")
	
	message(STATUS "Library OpenSSL not found :(")
	
endif()
